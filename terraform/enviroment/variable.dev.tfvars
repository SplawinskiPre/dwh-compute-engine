# --------------------------------------
# Configuration file for Development
# --------------------------------------

#basic project settings, copy direct from gcp, ask platformteam for a project
project = "hg-a0638-dwh-npr-b8e3"

#service account name, ask platformteam for an account 
service_account = "dwh-ce-admin@hg-a0638-dwh-npr-b8e3.iam.gserviceaccount.com"