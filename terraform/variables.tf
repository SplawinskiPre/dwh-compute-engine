
variable "name" {
  default = "test"
}

variable "machine_type" {
  default = "f1-micro"
}

variable "resource_name_prefix" {
  default = "hg"
}

# Region - do not change in the beginning
variable "region" {
  default = "europe-west1"
}

# Region - do not change in the beginning
variable "zone" {
  default = "europe-west1-b"
}